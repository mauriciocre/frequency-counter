
document.getElementById("countButton").onclick = function () {

    document.getElementById('lettersDiv').innerHTML = ""
    document.getElementById('wordsDiv').innerHTML = ""

    const typedText = document.getElementById("textInput").value;
    let myText;
    myText = typedText.toLowerCase();
    myText = myText.replace(/[^a-z'\s]+/g, "");

    let letterCounts = {};
    for (let i = 0; i < myText.length; i++) {
        currentLetter = myText[i];
        // faça algo com cada letra 
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
    }





    let array = [];

    for (let letter in letterCounts) {
        array[letter] = letterCounts[letter];
    }
    array.sort();
    console.log(array);
    

    // console.log(array.sort(function (a, b) {
    //     return a.value - b.value;
    // }));
    

    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }

    words = myText.split(/\s/);


    const wordsCounts = {};
    for (let i = 0; i < words.length; i++) {
        currentWord = words[i];
        if (wordsCounts[currentWord] === undefined) {
            wordsCounts[currentWord] = 1;
        } else {
            wordsCounts[currentWord]++;
        }
    }

    for (let word in wordsCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + wordsCounts[word] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);

    }
}

